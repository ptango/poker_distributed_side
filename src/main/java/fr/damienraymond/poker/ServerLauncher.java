package fr.damienraymond.poker;

import fr.adriencadoret.texasholdem.GameRunner;

/**
 * This class allows to launch the game
 */
public class ServerLauncher {

	public static void main(String[] args) {

		// It could be nice to use a kind of dependency injection here
		GameRunner gameRunner = new DistributedGameRunner();

		gameRunner.run();

	}

}
