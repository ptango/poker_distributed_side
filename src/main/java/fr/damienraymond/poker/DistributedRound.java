package fr.damienraymond.poker;

import fr.adriencadoret.texasholdem.Card;
import fr.adriencadoret.texasholdem.Player;
import fr.adriencadoret.texasholdem.Round;
import fr.damienraymond.poker.distributed.application.PokerServerInstance;
import fr.damienraymond.poker.poker.PlayerPokerMessage;
import fr.damienraymond.poker.poker.PokerMessage;
import fr.damienraymond.poker.utils.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by damien on 30/12/2015.
 */
public class DistributedRound extends Round {


    private DistributedGame distributedGame;
    private Map<Player, PokerServerInstance> instances;

    public DistributedRound(DistributedGame distributedGame, Map<Player, PokerServerInstance> instances) {
        super(distributedGame);
        this.distributedGame = distributedGame;
        this.instances = instances;
        this.notifyClients();
    }

    @Override
    public void run() {
        super.run();
        this.notifyClients();
    }

    @Override
    protected void betPhaseIter() {
        super.betPhaseIter();
        this.notifyClients();
    }

    @Override
    protected int readUserAmount() {
        Player currentPlayer = this.currentPlayer;
        PokerServerInstance pokerServerInstance = instances.get(currentPlayer); // TODO could fail
        return pokerServerInstance.readUserAmount();
    }

    @Override
    protected String askPlayerToPlay() {
        return this.askCurrentPlayerToPlay();
    }

    @Override
    protected void dealIter(Player player) {
        super.dealIter(player);
        this.notifyClients();
    }


    @Override
    public void betTurn() {
        super.betTurn();
        this.notifyClients();

    }

    @Override
    public void betRiver() {
        super.betRiver();
        this.notifyClients();
    }



    public void notifyClients(){
        PokerMessage pokerMessage = this.buildStateMessage();
        for (PokerServerInstance pokerServerInstance : instances.values()) {
            pokerServerInstance.updateInfo(pokerMessage);
        }
    }

    private PokerMessage buildStateMessage() {
        LinkedList<Player> gamePlayers  = this.game.getPlayers();
        LinkedList<Player> roundPlayers = this.getPlayers();

        int tablePot = this.getPot();
        List<Card> tableCards = this.getTableCards();
        String step = currentStep;
        int button = distributedGame.getDealer().getId();
        List<PlayerPokerMessage> players = gamePlayers.stream()
                .map(p -> {
                    boolean isFolded = !roundPlayers.contains(p);
                    return new PlayerPokerMessage(
                            p.getId(),
                            p.getName(),
                            p.getCredit(),
                            isFolded,
                            Arrays.asList(p.getCards())
                    );
                })
                .collect(Collectors.toList());
        return new PokerMessage(
                tablePot,
                tableCards,
                step,
                button,
                players
        );
    }

    public String askCurrentPlayerToPlay(){
        try {
            Logger.trace("sleep : askCurrentPlayerToPlay");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Player currentPlayer = this.currentPlayer;
        Logger.info("Current player is : " + currentPlayer);
        PokerServerInstance pokerServerInstance = instances.get(currentPlayer); // TODO could fail
        return pokerServerInstance.askPlayerToPlay();
    }
}
