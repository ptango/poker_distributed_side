package fr.damienraymond.poker.message;

/**
 * Created by damien on 26/11/2015.
 */
public class Message implements Serializable {

    private String message;

    private MessageType message_type;

    //private T content;

    public Message(String message, MessageType messageType) {
        this.message = message;
        this.message_type = messageType;
    }

    @Override
    public String serialize() {
        return MessageJsonConverter.toJson(this);
    }

    @Override
    public MessageType getMessageType() {
        return message_type;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", message_type=" + message_type +
                '}';
    }
}
