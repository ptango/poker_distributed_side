package fr.damienraymond.poker.message;

/**
 * Created by damien on 16/12/2015.
 */
public interface Serializable {
    String serialize();
    MessageType getMessageType();
}
