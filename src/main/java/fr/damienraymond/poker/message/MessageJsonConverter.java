package fr.damienraymond.poker.message;

import com.google.gson.Gson;

/**
 * Created by damien on 08/12/2015.
 */
public class MessageJsonConverter {

    public static <T> T fromJson(String s, Class<T> tClass){
        return new Gson().fromJson(s, tClass);
    }

    public static <T> String toJson(T m){
        return new Gson().toJson(m);
    }
}
