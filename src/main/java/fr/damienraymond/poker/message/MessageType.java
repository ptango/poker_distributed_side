package fr.damienraymond.poker.message;

/**
 * Created by damien on 08/12/2015.
 */
public enum MessageType {
    USER_AMOUNT,
    ASK_PLAYER_TO_PLAY,
    ASK_NAME,
    UPDATE_INFO,
    NOTHING
}
