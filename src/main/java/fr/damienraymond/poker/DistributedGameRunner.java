package fr.damienraymond.poker;


import fr.adriencadoret.texasholdem.Game;
import fr.adriencadoret.texasholdem.GameRunner;
import fr.adriencadoret.texasholdem.Player;
import fr.damienraymond.poker.distributed.application.PokerServerInstance;
import fr.damienraymond.poker.distributed.features.ConnectionManagement;
import fr.damienraymond.poker.distributed.link.Server;
import fr.damienraymond.poker.message.Message;
import fr.damienraymond.poker.utils.Logger;

import java.net.Socket;
import java.util.*;

/**
 * This class allows to manage the game progress. 
 */
public class DistributedGameRunner extends GameRunner implements ConnectionManagement {

	private Map<Player,PokerServerInstance> instances;

	public DistributedGameRunner() {
		instances = new HashMap<>();
	}

	@Override
	protected void addPlayers() {
		int port = Constants.PORT;
		Server<Message> server = new Server<>(port, this);
		new Thread(server).start();

		Logger.info("Press enter to start game");
		new Scanner(System.in).nextLine();
		//server.stop();
	}

	@Override
	protected Game buildGame(int smallBlind, int initialCash) {
		return new DistributedGame(smallBlind, initialCash, instances);
	}

	@Override
	public void manageNewConnection(Socket s) {
		PokerServerInstance pokerServerInstance = new PokerServerInstance(s, this);
		new Thread(pokerServerInstance).start();
		pokerServerInstance.askName();

		Logger.debug("New client registered...");
	}


	public void setNameAndRegisterPlayers(Player p, PokerServerInstance psi){
		this.getGame().addPlayer(p);
		instances.put(p, psi);
	}



	public Game getGame() {
		return game;
	}





}
