package fr.damienraymond.poker;

import fr.adriencadoret.texasholdem.GameRunner;
import fr.damienraymond.poker.distributed.application.PokerClient;

/**
 * This class allows to launch the game
 */
public class ClientLauncher {

	public static void main(String[] args) {
		PokerClient pokerClient = new PokerClient();
		pokerClient.askName();
		pokerClient.init();
	}

}
