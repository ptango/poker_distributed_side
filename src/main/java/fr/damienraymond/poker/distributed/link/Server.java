package fr.damienraymond.poker.distributed.link;

import fr.damienraymond.poker.distributed.features.ConnectionManagement;
import fr.damienraymond.poker.distributed.features.DistributedProvidedServices;
import fr.damienraymond.poker.utils.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by damien on 15/12/2015.
 */
public class Server<T extends fr.damienraymond.poker.message.Serializable> implements Runnable {


    private ConnectionManagement cm;

    private ServerSocket s;

    private final int PORT;

    private boolean goOn = true;

    public Server(int port, ConnectionManagement cm) {
        this.PORT = port;
        this.cm = cm;
    }

    public void stop(){
        this.goOn = false;
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init(){
        try {
            s = new ServerSocket(PORT);
            Logger.debug("Start sever at " + PORT);

            while(goOn){
                Logger.info("Waiting for clients");
                Socket clientSocket = s.accept();
                cm.manageNewConnection(clientSocket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.info("Stop server");
    }

    @Override
    public void run() {
        this.init();
    }
}
