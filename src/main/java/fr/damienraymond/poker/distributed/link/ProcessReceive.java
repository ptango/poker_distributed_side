package fr.damienraymond.poker.distributed.link;

import fr.damienraymond.poker.distributed.features.DistributedProvidedServices;
import fr.damienraymond.poker.distributed.features.DistributedRequiredServices;
import fr.damienraymond.poker.message.Serializable;
import fr.damienraymond.poker.utils.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by damien on 22/12/2015.
 */
public interface ProcessReceive<T extends Serializable> extends DistributedProvidedServices<T> {
    default void processReceive(DistributedRequiredServices drs, Socket socket){
        try {
            Logger.debug("processReceive()");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String input = bufferedReader.readLine();
            drs.receive(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
