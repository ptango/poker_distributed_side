package fr.damienraymond.poker.distributed.link;

import fr.damienraymond.poker.distributed.features.DistributedProvidedServices;
import fr.damienraymond.poker.distributed.features.DistributedRequiredServices;
import fr.damienraymond.poker.message.Serializable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by damien on 26/11/2015.
 */
public class ServerInstance<T extends Serializable> implements Runnable, ProcessReceive<T> {

    private Socket socket;
    private OutputStream outputStream;
    private DistributedRequiredServices drs;

    public ServerInstance(Socket socket, DistributedRequiredServices drs){
        this.drs = drs;
        this.socket = socket;
        try {
            outputStream = this.socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        while(true){
            processReceive(drs, socket);
        }
    }


    @Override
    public OutputStream getOutputStream() {
        return this.outputStream;
    }
}
