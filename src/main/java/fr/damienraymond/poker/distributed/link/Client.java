package fr.damienraymond.poker.distributed.link;

import fr.damienraymond.poker.distributed.features.DistributedProvidedServices;
import fr.damienraymond.poker.distributed.features.DistributedRequiredServices;
import fr.damienraymond.poker.message.Serializable;
import fr.damienraymond.poker.utils.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by damien on 16/12/2015.
 */
public class Client<T extends Serializable> implements ProcessReceive<T>, Runnable {

    private DistributedRequiredServices drs;

    private Socket socket;
    private OutputStream outputStream;


    public Client(String host, int port, DistributedRequiredServices drs) {
        this.drs = drs;
        try {
            this.socket = new Socket(host, port);
            Logger.debug("Connected to " + host + ":" + port);
            outputStream = this.socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Logger.info("Listening");
        while(true){
            processReceive(drs, socket);
        }
    }

    @Override
    public OutputStream getOutputStream() {
        return this.outputStream;
    }
}
