package fr.damienraymond.poker.distributed.features;

/**
 * Created by damien on 26/11/2015.
 */
public interface DistributedRequiredServices {
    void receive(String input);
}
