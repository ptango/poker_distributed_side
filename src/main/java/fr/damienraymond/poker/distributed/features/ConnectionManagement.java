package fr.damienraymond.poker.distributed.features;

import java.net.Socket;

/**
 * Created by damien on 16/12/2015.
 */
public interface ConnectionManagement {
    void manageNewConnection(Socket s);
}
