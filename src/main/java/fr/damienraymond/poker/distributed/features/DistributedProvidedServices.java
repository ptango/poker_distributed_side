package fr.damienraymond.poker.distributed.features;

import fr.damienraymond.poker.message.Serializable;
import fr.damienraymond.poker.utils.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Created by damien on 26/11/2015.
 */
public interface DistributedProvidedServices<T extends Serializable> {
    default void send(T m) {
        OutputStream outputStream = getOutputStream();
        if (outputStream != null){
            PrintWriter printWriter = new PrintWriter(outputStream);
            String stringToSend = m.serialize();
            try {
                printWriter.println(stringToSend);
                //outputStream.write(stringToSend.getBytes());
                //outputStream.write('\n');
                printWriter.flush();
                Logger.trace("Sent : " + m.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    OutputStream getOutputStream();
}
