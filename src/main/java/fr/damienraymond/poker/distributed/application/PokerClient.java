package fr.damienraymond.poker.distributed.application;

import fr.adriencadoret.texasholdem.Player;
import fr.damienraymond.poker.Constants;
import fr.damienraymond.poker.distributed.features.DistributedRequiredServices;
import fr.damienraymond.poker.distributed.link.Client;
import fr.damienraymond.poker.message.Message;
import fr.damienraymond.poker.message.MessageJsonConverter;
import fr.damienraymond.poker.message.MessageType;
import fr.damienraymond.poker.poker.PokerMessage;
import fr.damienraymond.poker.utils.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Scanner;

/**
 * Created by damien on 08/12/2015.
 */
public class PokerClient implements DistributedRequiredServices {

    private String name = null;
    private Client<Message> dps;


    public void askName(){
        String tmp;

        while(this.name == null){
            Scanner scanner = new Scanner(System.in);

            Logger.info("Please, enter a player name : ");
            tmp = scanner.nextLine();

            if(tmp != null){
                this.name = tmp;
                Logger.trace("Registered name : " + tmp);
            } else{
                Logger.error("Please, enter a player name : ");
            }

        }
    }

    public void init(){
        String host = Constants.HOST;
        int port = Constants.PORT;
        dps = new Client<>(host, port, this);
        new Thread(dps).start();
    }



    @Override
    public void receive(String input) {
        Logger.trace("received " + input);
        try{
            JSONObject jsonObject = new JSONObject(new JSONTokener(input));

            MessageType message_type = MessageType.valueOf((String) jsonObject.get("message_type"));

            if(message_type.equals(MessageType.UPDATE_INFO)){
                PokerMessage pokerMessage = MessageJsonConverter.fromJson(input, PokerMessage.class);
                printGameInfo(pokerMessage);
            }else{
                Message message = MessageJsonConverter.fromJson(input, Message.class);
                if(message.getMessageType() == MessageType.ASK_NAME){
                    dps.send(new Message(this.name, MessageType.ASK_NAME));
                }else if(message.getMessageType() == MessageType.ASK_PLAYER_TO_PLAY){
                    String choice = this.askPlayerToPlay();
                    Logger.debug("Your choice is " + choice);
                    dps.send(new Message(choice, MessageType.ASK_PLAYER_TO_PLAY));
                }else if(message.getMessageType() == MessageType.USER_AMOUNT){
                    int choice = this.readUserAmount();
                    Logger.debug("Your choice is " + choice);
                    dps.send(new Message(Integer.toString(choice), MessageType.USER_AMOUNT));
                }
            }
        }catch (Exception e){
            Logger.error("Error occurred in receive", e);
            System.exit(1); // to prevent infinite loop
        }


    }

    private int readUserAmount() {
        Scanner amountScanner;
        Logger.info("Enter raise amount:");
        amountScanner = new Scanner(System.in);
        return amountScanner.nextInt();
    }

    private void printGameInfo(PokerMessage pokerMessage) {
        Logger.info(pokerMessage.toString(name));
    }

    private String askPlayerToPlay() {
        Logger.info("It's your turn ! \n"+
                "You can: 'FOLD', 'CALL', 'RAISE', 'ALLIN', 'CHECK' OR 'BET' ");
        return new Scanner(System.in).nextLine();
    }
}
