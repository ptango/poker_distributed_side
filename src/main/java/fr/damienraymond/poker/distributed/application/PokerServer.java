package fr.damienraymond.poker.distributed.application;

import fr.damienraymond.poker.Constants;
import fr.damienraymond.poker.distributed.features.ConnectionManagement;
import fr.damienraymond.poker.distributed.link.Server;
import fr.damienraymond.poker.message.Message;

import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by damien on 26/11/2015.
 */
public class PokerServer implements ConnectionManagement {

//    public abstract void addPlayer(Message m);
//    public abstract void removePlayer(Message m);

//    private DistributedProvidedServices<T> server;
    private Server<Message> server;

    private List<PokerServerInstance> instances; // TODO Convert to map to know the link between client name and poker instance

    public PokerServer(){
        int port = Constants.PORT;
        this.server = new Server<>(port, this);
        this.instances = new LinkedList<>();
    }

    public void init(){
        this.server.init();
        //new Thread(this.server).start();
    }


    @Override
    public void manageNewConnection(Socket s) {
        PokerServerInstance pokerServerInstance = new PokerServerInstance(s, null);
        pokerServerInstance.init();
        pokerServerInstance.repl();
        instances.add(pokerServerInstance);
    }
}
