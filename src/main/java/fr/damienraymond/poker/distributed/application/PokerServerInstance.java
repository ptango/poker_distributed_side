package fr.damienraymond.poker.distributed.application;

import fr.adriencadoret.texasholdem.Player;
import fr.damienraymond.poker.DistributedGameRunner;
import fr.damienraymond.poker.distributed.features.DistributedRequiredServices;
import fr.damienraymond.poker.distributed.link.ServerInstance;
import fr.damienraymond.poker.message.Message;
import fr.damienraymond.poker.message.MessageJsonConverter;
import fr.damienraymond.poker.message.MessageType;
import fr.damienraymond.poker.message.Serializable;
import fr.damienraymond.poker.poker.PokerMessage;
import fr.damienraymond.poker.utils.Logger;

import java.net.Socket;
import java.util.Scanner;

/**
 * Created by damien on 08/12/2015.
 */
public class PokerServerInstance implements DistributedRequiredServices, Runnable {

    private DistributedGameRunner gameRunner;
    private ServerInstance<Serializable> dps;

    private String response;
    private int userAmount;

    public PokerServerInstance(Socket s, DistributedGameRunner gameRunner) {
        this.gameRunner = gameRunner;
        this.dps = new ServerInstance<>(s, this);
    }

    public void askName(){
        Logger.debug("Ask client name");
        Message name = new Message("name", MessageType.ASK_NAME);
        dps.send(name);
    }

    public void init(){
        new Thread(this.dps).start();
    }

    public void repl(){
        while(true){
            Scanner reader = new Scanner(System.in);
            Logger.info("Write something : ");
            Message message = new Message(reader.nextLine(), MessageType.NOTHING);
            dps.send(message);
        }
    }

    @Override
    public void receive(String input) {
        Message message = MessageJsonConverter.fromJson(input, Message.class);
        Logger.trace("received " + input);
        if (message.getMessageType() == MessageType.ASK_NAME){
            String name = message.getMessage();

            Player player = new Player(name);
            gameRunner.setNameAndRegisterPlayers(player, this);
        }else if(message.getMessageType() == MessageType.ASK_PLAYER_TO_PLAY){
            response = message.getMessage();
            synchronized (this){
                Logger.debug("notify");
                this.notify();
            }
            Logger.info("Player choice : " + response);
        }else if(message.getMessageType() == MessageType.USER_AMOUNT){
            userAmount = Integer.parseInt(message.getMessage());
            synchronized (this){
                Logger.debug("notify");
                this.notify();
            }
            Logger.info("Player amount : " + response);
        }
    }

    public String askPlayerToPlay() {
        response = null;
        dps.send(new Message("", MessageType.ASK_PLAYER_TO_PLAY));
        waitForResponse();
        return response;
    }

    private void waitForResponse() {
        synchronized (this){
            try {
                Logger.debug("wait");
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        this.init();
    }

    public void updateInfo(PokerMessage pokerMessage) {
        dps.send(pokerMessage);
    }

    public int readUserAmount() {
        userAmount = 0;
        dps.send(new Message("", MessageType.USER_AMOUNT));
        waitForResponse();
        return userAmount;
    }
}
