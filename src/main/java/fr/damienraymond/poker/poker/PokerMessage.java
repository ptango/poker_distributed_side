package fr.damienraymond.poker.poker;

import fr.adriencadoret.texasholdem.Card;
import fr.adriencadoret.texasholdem.Player;
import fr.damienraymond.poker.message.MessageJsonConverter;
import fr.damienraymond.poker.message.MessageType;
import fr.damienraymond.poker.message.Serializable;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by damien on 27/12/2015.
 */

/*
{
__"game": {
____"table": {
______"pot": 0,
______"cards": ["7T", "10P"]
____},
____"step": "RIVER",
____"button": "12ab97f5-b043-465f-b7fa-70e86836ac7c", // playerId
____"small_blind": "12ab97f5-b043-465f-b7fa-70e86836ac7c",
____"big_blind": "12ab97f5-b043-465f-b7fa-70e86836ac7c"
__},
__"players": [
____{
______"id": "12ab97f5-b043-465f-b7fa-70e86836ac7c", // delivered by server
______"name": "player_name",
______"amountThePlayerHas": 0,
______"isFolded": false,
______"cards": ["7T", "10P", "10P", "10P", "10P"] // OPTION
____}
____//, ...
__]
}
 */
public class PokerMessage implements Serializable {

    private MessageType message_type = getMessageType();

    private int tablePot;
    private List<Card> tableCards;
    private String step;
    private int button;
    private List<PlayerPokerMessage> players;


    public PokerMessage(int tablePot, List<Card> tableCards, String step, int button, List<PlayerPokerMessage> players) {
        this.tablePot = tablePot;
        this.tableCards = tableCards;
        this.step = step;
        this.button = button;
        this.players = players;
    }

    @Override
    public String serialize() {
        return MessageJsonConverter.toJson(this);
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.UPDATE_INFO;
    }

    public String toString(String playerName) {
//        Creates bug
//        String dealer = players.stream()
//                .filter(p -> p.getId() == button)
//                .map(PlayerPokerMessage::getName)
//                .findFirst()
//                .orElse("no dealer found");

        String dealer = "no dealer found";
        for (PlayerPokerMessage player : players) {
            if (player.getId() == button){
                dealer = player.getName();
                break;
            }
        }

        StringBuilder p = new StringBuilder();
        for (PlayerPokerMessage player : players) {
            p.append("\n");
            if(player.getName().equals(playerName)){
                p.append(player);
            }else{
                p.append(player.toStringWithoutCard());
            }
        }
        p.append("\n");


        step = Objects.equals(step, "") ? "no step" : step;

        return "Step : " + step + " ; Table($" + tablePot + ") Cards(" + tableCards + ") ; Dealer : " + dealer + " ; players : " + p;
    }
}
