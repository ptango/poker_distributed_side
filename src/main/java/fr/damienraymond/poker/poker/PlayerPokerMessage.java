package fr.damienraymond.poker.poker;

import fr.adriencadoret.texasholdem.Card;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by damien on 28/12/2015.
 */

/*
{
__"id": "12ab97f5-b043-465f-b7fa-70e86836ac7c", // delivered by server
__"name": "player_name",
__"amountThePlayerHas": 0,
__"isFolded": false,
__"cards": ["7T", "10P", "10P", "10P", "10P"] // OPTION
}
 */
public class PlayerPokerMessage {
    private int id;
    private String name;
    private int amountThePlayerHas;
    private boolean isFolded;
    private List<Card> cards;

    public PlayerPokerMessage(int id, String name, int amountThePlayerHas, boolean isFolded, List<Card> cards) {
        this.id = id;
        this.name = name;
        this.amountThePlayerHas = amountThePlayerHas;
        this.isFolded = isFolded;
        this.cards = cards;
    }

    @Override
    public String toString() {
        String fold = isFolded ? "folded" : "not folded";
        return name + "($" + amountThePlayerHas + ") " + fold + " Cards(" + cards + ")";
    }

    public String toStringWithoutCard() {
        String fold = isFolded ? "folded" : "not folded";
        return name + "($" + amountThePlayerHas + ") " + fold;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
