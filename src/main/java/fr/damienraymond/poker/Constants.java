package fr.damienraymond.poker;

/**
 * Created by damien on 08/12/2015.
 */
public final class Constants {

    public static final String HOST =
            System.getProperty("server.host") == null ?
                    "localhost" :
                    System.getProperty("server.host");

    public static final int PORT =
            System.getProperty("server.port") == null ?
                    8025 :
                    Integer.parseInt(System.getProperty("server.port"));

    public static final String PATH =
            System.getProperty("server.path") == null ?
                    "/websocket" :
                    System.getProperty("server.path");
}
