package fr.damienraymond.poker;

import fr.adriencadoret.texasholdem.Game;
import fr.adriencadoret.texasholdem.Player;
import fr.adriencadoret.texasholdem.Round;
import fr.damienraymond.poker.distributed.application.PokerServerInstance;
import fr.damienraymond.poker.utils.Logger;

import java.util.Map;

/**
 * Created by damien on 30/12/2015.
 */
public class DistributedGame extends Game {

    private Map<Player, PokerServerInstance> instances;

    public DistributedGame(int smallBlind, int startingCash, Map<Player, PokerServerInstance> instances) {
        super(smallBlind, startingCash);
        this.instances = instances;
    }


    @Override
    public Round createRound() {
        this.changeDealer();
        return new DistributedRound(this, instances);
    }

    @Override
    public void setDealer(Player dealer) {
        // TODO SERVER : Notify clients -> useless
        //Logger.warn("TODO SERVER : Notify clients");
        super.setDealer(dealer);
    }
}
