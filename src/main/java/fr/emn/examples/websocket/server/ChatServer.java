package fr.emn.examples.websocket.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.glassfish.tyrus.server.Server;

public class ChatServer {

	public static final String HOST = System.getProperty("server.host") == null ? "localhost"
			: System.getProperty("server.host");
	public static final int PORT = System.getProperty("server.port") == null ? 8025
			: Integer.parseInt(System.getProperty("server.port"));
	public static final String PATH = System.getProperty("server.path") == null ? "/websocket"
			: System.getProperty("server.path");
	
	public static void main(String[] args) {
			
		    Server server = new Server(HOST, 
		    		PORT, 
		    		PATH, null, ChatEndpoint.class);
		 
		    try {
		        server.start();
		        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		        System.out.print("Please press a key to stop the server.");
		        reader.readLine();
		    } catch (Exception e) {
		        e.printStackTrace();
		    } finally {
		        server.stop();
		    }
		

	}

}
