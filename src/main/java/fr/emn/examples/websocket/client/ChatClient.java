package fr.emn.examples.websocket.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;

import fr.emn.examples.websocket.server.ChatServer;

public class ChatClient {
	private static CountDownLatch messageLatch;

	public static void main(String[] args) {
		try {
			messageLatch = new CountDownLatch(1);

			final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();

			ClientManager client = ClientManager.createClient();
			Session sess = client.connectToServer(new Endpoint() {

				@Override
				public void onOpen(Session session, EndpointConfig config) {

					session.addMessageHandler(new MessageHandler.Whole<String>() {

						public void onMessage(String message) {
							System.out.println("Received message: " + message);
							messageLatch.countDown();
						}
					});

				}
			}, cec, new URI("ws://" + ChatServer.HOST + ":" + ChatServer.PORT + ChatServer.PATH + "/chat"));

			
			System.out.println("Your message: ");
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String s;
			
			while ((s = in.readLine()) != null && s.length() != 0) {
				sess.getBasicRemote().sendText(s);
				messageLatch.await(100, TimeUnit.SECONDS);
				System.out.println("Your message: ");
			}

			in.close();

			client.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
