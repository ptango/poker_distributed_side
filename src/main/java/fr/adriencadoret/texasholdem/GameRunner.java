package fr.adriencadoret.texasholdem;


import fr.damienraymond.poker.utils.Logger;

import java.util.Scanner;

/**
 * This class allows to manage the game progress. 
 * @author Cadoret Adrien
 *
 */
public class GameRunner {
	
	/**
	 * The game related to this
	 */
	protected Game game; // Need to change this propriety visibility to use it in child classes

	/**
	 * Constructor
	 */
	public GameRunner(){
	}
	
	/**
	 * Runs the game and progresses it
	 */
	public void run(){
		
		Round round;
		
		// Initialize the whole game 
		createGame();
		addPlayers();
				 
		Logger.info(game.getPlayers().toString());
		
		checkStartGame();
	
		while(!game.hasWinner()){
			round = game.createRound();
			round.run();
			game.setRoundNumber(game.getRoundNumber()+1);
			game.getGameStatistiks();
		}
		
		Logger.info("THE WINNER IS : "+game.getWinner().getName());
		
		endGame();
				 			 
	}

	/**
	 * Allows to restart a game or quit the console
	 */
	private void endGame() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        String answer;
		
        Logger.info("The game is done. Do you want to restart ? (Say 'YES' or 'NO')");
        answer = scanner.nextLine();
        if(answer.equals("YES")){
        	this.run();
        }
        else if(answer.equals("NO")){
        	Logger.info("Thanks you for playing ! Bye bye");
        	System.exit(0);
        }
        else{
        	this.endGame();
        }
        
        
		
	}

	private void checkStartGame() {
		if(game.getPlayers().size()<2){
			Logger.info("A game must have at less 2 players");
			addPlayers();
		}
	}


	// Private here causes trouble in case of inheritance
	// private  void addPlayers() {
	protected void addPlayers() {

		boolean addingPlayer = true;
		String name;
		
		while(addingPlayer){
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
	        
	        Logger.info("Enter a player name:  (Enter 'start' to run game)");
	        name=scanner.nextLine();
	        
	        if(name.equals("start")){
	        	addingPlayer = false;
	        }
	        else if(name instanceof String){
		        Player player = new Player(name);
		        game.addPlayer(player);
	        }
	        else{
	        	Logger.info("Please enter a string");
	        }

		}
		
		
	}

	private  void createGame() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
        int smallBlind=0, initialCash=0;
        
        Logger.info("Enter Small Blind:");
        smallBlind=scanner.nextInt();
        scanner.nextLine(); //This is needed to pick up the new line
        Logger.info("By default, the Big Blind equals to: "+smallBlind*2);
        while(initialCash < smallBlind*2){
            Logger.info("Enter Initial Cash gived to each player (must be > bigBlind):");
            initialCash=scanner.nextInt();
        }
		game = buildGame(smallBlind, initialCash);
	}

	/**
	 * Need to extract this piece of code to be able to override it
     */
	protected Game buildGame(int smallBlind, int initialCash) {
		return new Game(smallBlind, initialCash);
	}


}
