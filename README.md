# Poker
This project is an implementation of Poker Hold'em game in Java.

## How to use ?
Import project in your favorite IDE.

**Be sure that maven is managed. The project has a dependencies.**


## My work each week

- **S53** : I mainly worked on the adaptation of Adrien's code. The difficult part was to avoid changes in his code. The next think to do is to create, send, and wait for response messages.
- **S52** : This week I focused on optimize code of the distributed part (e.g. : generalisation via interfaces, etc.) 
- **S51** : I've worked on the architecture of the distributed part. My objective was to code the distributed part for today. In fact, it almost finished. 
- **S50** : Create structure of the distributed part.


## What should I put in messages for clients ?

The state of the game : cf state.json
